<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'José Felipe da Silva',
            'email' => 'jose.felipe@kinghost.com.br',
            'password' => bcrypt('rada85'),
        ]);
    }
}
