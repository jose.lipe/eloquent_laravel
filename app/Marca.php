<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $fillable = ['name'];
    public $timestamps = false;

    public function texts()
    {
        return $this->morphedByMany(Text::class, 'marcado');
    }

    public function images()
    {
        return $this->morphedByMany(Image::class,'marcado');
    }
}
