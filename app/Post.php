<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $fillable = ['title','content', 'type'];
    protected $dates = ['deleted_at'];

    // Proteger campos
    //protected $guarded = ['id', 'created_at'];

    // Mudar nome da tabela
    //protected $table = 'post';

    // Mudar chave primaria
    //protected $primaryKey = 'post_id';

    // Tirar campos timestamps
    //public $timestamps = false;

    // Trocar a conexão do Model
    //protected $connection = 'sqlite';

    // Instanciar objeto Carbon em algum campo
    //protected $dates = ['created_at', 'update_at','personalizado'];

    // casting de atributos
    /*protected $cast = [
      'campo' => 'boolean',
      'numero' => 'integer'
    ];*/

    // Exemplo de escopo global
    /*protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('image', function (Builder $builder){
            $builder->where('type','image');
        });
    }*/

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = strtolower($value);
    }

    public function getTitleAttribute($value)
    {
        return ucfirst($value);
    }

    public function scopeOfType($query, $type)
    {
        return $query->where('type',$type);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
