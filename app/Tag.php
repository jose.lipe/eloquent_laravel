<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name'];
    public $timestamps = false;
    
    public function posts()
    {
        return $this->belongsToMany(Post::class); // caso tenha campos na tabela pivo
                                                         // ->withTimestamps() para timestamps
                                                         // ->withPivot('campo1', 'campo2') para qualque campo
    }
}
