<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['description', 'resolution'];

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function marcas()
    {
        return $this->morphToMany(Marca::class,'marcado');
    }
}
